import express from 'express'
import bodyParser from 'body-parser'
import dotenv from 'dotenv'
import {Client, DMChannel, Message, MessageAttachment, TextChannel} from 'discord.js-selfbot-v13'
import { ChannelTypes } from 'discord.js-selfbot-v13/typings/enums'

if(isNaN(parseInt(process.argv[2])))
    throw new Error('Please specify a number as the first program argument to define the port number.')
const port = parseInt(process.argv[2])

if(!process.argv[3])
    throw new Error('Please specify the name of the communication module as the second program argument. The name has to equal the name of the respective connector in DPEX')
const communicationModule = process.argv[3]

dotenv.config()

const receiveMessageEndpoint = process.env.RECEIVE_MESSAGE_ENDPOINT
if(!receiveMessageEndpoint || receiveMessageEndpoint === '')
    throw new Error('Please specify the relative URL of the endpoint in the dpex framwork to receive a message as an environment variable named "RECEIVE_MESSAGE_ENDPOINT"')

const app = express()
app.use(bodyParser.json({limit: '1000mb'}))
app.use(bodyParser.urlencoded({limit: '1000mb', extended: true}))

//mapping an access token to a discord client object
const clientMap = new Map<string, Client>()

app.listen(port, async () => {
    console.log(`DPEX-communication-discord-adapter listening on port ${port}`)
})

/**
 * This module unfortunately provides no proper login functionality because of discord's restrictions concerning self-botting.
 * Instead the user has to log into discord in the browser and copy its access token from the network tab.
 * This request will only create the client object
 * body:
 * {
 *     userId: string
 *     accessToken: string
 * }
 */
app.post('/login', async (req, res) => {
    const accessToken: string = req.body.accessToken
    const userId: string = req.body.userId

    if(!accessToken || !userId)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following fields: accessToken, userId')

    try{
        var client = new Client({checkUpdate: false})
        client.login(accessToken).catch(err => {
            res.status(500).send('Login failed: ' + err.message)
        })
    }
    catch(error: any){
        return res.status(403).send('Login failed: ' + error.message)
    }

    clientMap.set(accessToken, client)

    client.once('ready', () => {
        if(client.user?.id !== userId){
            res.status(403).send('Invalid user ID, access token combination')
            return
        }
        console.log(`${client.user?.username} is ready`)
        res.json({
            username: client.user?.username
        })
    })

    client.on('messageCreate', async message => {
        handleTimelineEvent(message, client)
    })
})

async function handleTimelineEvent(message: Message, client: Client){
    if(message.author.id === client.user!.id)
        return
    type ReceiveMessageBody = {
        message: string,
        sender: string,
        recipientAccessToken: string,
        roomId: string,
        timestamp: number,
        communicationModule: string
        fileUrl?: string|null
        fileIsImage?: boolean
    }

    const body: ReceiveMessageBody = {
        message: message.content,
        sender: message.author.id,
        recipientAccessToken: client.token!,
        roomId: message.channelId,
        timestamp: message.createdTimestamp,
        //this has to be called like the communicationFactory in dpex!
        communicationModule
    }
    if(message.attachments.size > 0){
        const attachment = message.attachments.first()!
        body.fileUrl = attachment.url
        body.fileIsImage = attachment.contentType?.includes('image')
        body.message = attachment.name? attachment.name : 'file'
    }
    const requestOptions = {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
            'Content-Type': 'application/json'
        }
    }
    try{
        const response = await fetch(receiveMessageEndpoint!, requestOptions)
        if(!response.ok)
            console.error(`Error handling incoming message: ${await response.text()} (${response.status})`)
    }
    catch(error){
        console.error(error)
    }
}

/**
 * body:
 * {
 *     accessToken: string
 * }
 */
app.delete('/logout', async (req, res) => {
    const accessToken: string = req.body.accessToken
    if(!accessToken)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following field: accessToken')

    const client = clientMap.get(accessToken)
    if(!client)
        return res.status(400).send('Unknown access token')

    const username = client.user?.username

    try{
        await client.logout()
        clientMap.delete(accessToken)
    }
    catch(error: any){
        res.status(500).send(error.message)
    }

    res.send('Successfully logged out '+username)
})

/**
 * headers:
 *     authorization: 'Bearer [accessToken]'
 * params:
 *     roomId: string
 */
app.get('/roomMessages/:roomId/', async (req, res) => {
    const roomId = req.params.roomId
    try{
        var client = authenticateUser(req.headers.authorization)
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }
    if(!roomId)
        return res.status(400).send('Request parameter \'roomId\' missing')

    const channel = await client.channels.fetch(roomId)
    if(!channel)
        return res.status(400).send('Unknown room ID')

    if(channel.type !== 'GUILD_TEXT' && channel.type !== 'DM')
        return res.status(501).send('Channel type not supported')

    type MessageObject = {
        message: string,
        fileUrl?: string|null,
        fileIsImage?: boolean,
        sender: string,
        timestamp: number
    }

    const messageObjects: MessageObject[] = []
    let numMessagesOfLastLoop = 100
    let lastMessage: string = ''
    let firstLoop = true
    while(numMessagesOfLastLoop === 100){
        numMessagesOfLastLoop = 0
        if(firstLoop){
            var messages = await channel.messages.fetch({limit: 100})
            firstLoop = false
        }
        else
            var messages = await channel.messages.fetch({limit: 100, before: lastMessage})
        for(const message of messages){
            numMessagesOfLastLoop++
            lastMessage = message[0]
            const messageObject: MessageObject = {
                message: message[1].attachments.first()? (message[1].attachments.first()!.name? message[1].attachments.first()!.name! : 'file') : message[1].content,
                sender: message[1].author.id,
                timestamp: message[1].createdTimestamp,
                fileUrl: message[1].attachments.first()?.url,
                fileIsImage: message[1].attachments.first()?.contentType?.includes('image')
            }
            messageObjects.push(messageObject)
        }
    }
    res.json(messageObjects.reverse())
})

/**
 * headers:
 *     authorization = 'Bearer [accessToken]'
 * params:
 *     roomId: string
 */
app.get('/roomMembers/:roomId', async (req, res) => {
    const roomId = req.params.roomId
    try{
        var client = authenticateUser(req.headers.authorization)
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }
    if(!roomId)
        return res.status(400).send('Request parameter \'roomId\' missing')

    const channel = await client.channels.fetch(roomId)
    if(!channel)
        return res.status(400).send('Unknown room ID')

    switch(channel.type){
        case 'GUILD_TEXT':
            const textChannel = channel as TextChannel
            const members: string[] = []
            for(const permission of textChannel.permissionOverwrites.cache){
                if(permission[1].type === 'member')
                    members.push(permission[0])
            }
            res.json(members)
            break
        case 'DM':
            const dmChannel = channel as DMChannel
            res.json([dmChannel.recipient.id])
            break
        default:
            res.status(501).send('Channel type not supported')
    }
})

/**
 * headers:
 *     authorization: 'Bearer [accessToken]'
 * body:
 * {
 *     recipient: string,
 *     message: string,
 *     file?: string (base64),
 *     fileIsImage?: boolean
 * }
 */
app.post('/sendDirectMessage', async (req, res) => {
    const recipient: string = req.body.recipient
    const message: string = req.body.message
    const file: string = req.body.file
    const fileIsImage: boolean = req.body.fileIsImage

    try{
        var client = authenticateUser(req.headers.authorization)
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }

    if(!recipient || !message)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following fields: recipient, message')

    const sender = client.user!.id

    //This is not an error case because this might indeed happen 'intentionally' when sending messages using org queries
    if(sender === recipient)
        return res.send('Sender equals recipient. Message not sent')

    try{
        const user = await client.users.fetch(recipient)
        if(file){
            const decodedFile = Buffer.from(file, 'base64')
            const attachment = new MessageAttachment(decodedFile, message)
            await user.send({files: [attachment]})
        }
        else
            await user.send(message)
        res.send()
    }
    catch(error){
        return res.status(500).send(error)
    }
})

/**
 * headers:
 *     authorization: 'Bearer [accessToken]'
 * body:{
 *     roomId: string,
 *     message: string,
 *     file?: string (base64),
 *     fileIsImage?: boolean
 * }
 */
app.post('/sendRoomMessage', async (req, res) => {
    const roomId: string = req.body.roomId
    const message: string = req.body.message
    const file: string = req.body.file
    const fileIsImage: boolean = req.body.fileIsImage

    try{
        var client = authenticateUser(req.headers.authorization)!
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }

    if(!roomId || !message)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following fields: roomId, message')

    try{
        const channel = await client.channels.fetch(roomId)
        if(!channel)
            return res.status(400).send('Unknown room ID')

        if(channel.type !== 'GUILD_TEXT' && channel.type !== 'DM')
            return res.status(501).send('Channel type not supported')

        if(file){
            const decodedFile = Buffer.from(file, 'base64')
            const attachment = new MessageAttachment(decodedFile, message)
            await channel.send({files: [attachment]})
        }
        else
            await channel.send(message)

        res.send()
    }
    catch(error){
        res.status(500).send(error)
    }
})

/**
 * headers:
 *     authorization: 'Bearer [accessToken]'
 * body:
 * {
 *     name: string,
 *     invite: string[],
 *     gar: string,
 *     serverId: string
 * }
 * response:
 * {
 *     spaceId: string
 * }
 */
app.post('/createSpace', async (req, res) => {
    const name: string = req.body.name
    const invite: string[] = req.body.invite
    const gar: string = req.body.gar
    const serverId: string = req.body.serverId

    try{
        var client = authenticateUser(req.headers.authorization)!
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }

    if(!name || !invite || !gar || !serverId)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following fields: name, invite, gar, serverId')

    //Don't create a second category (space) for an alliance
    const rooms = await getRooms(client, serverId)
    for(const room of rooms)
        if(room.gar === gar)
            return res.status(400).send('A space with this gar already exists')

    try{
        const guild = await client.guilds.fetch(serverId)
        const category = await guild.channels.create(`${name}|${gar}`, {type: ChannelTypes.GUILD_CATEGORY})
        const everyoneRole = guild.roles.everyone
        const spaceId = category.id
        
        await category.permissionOverwrites.create(client.user!.id, {
            'VIEW_CHANNEL': true,
        })
        for(const participant of invite){
            if(participant === client.user!.id)
                continue
            await category.permissionOverwrites.create(await client.users.fetch(participant), {
                'VIEW_CHANNEL': true,
            })
        }
        await category.permissionOverwrites.create(everyoneRole, {
            'VIEW_CHANNEL': false
        })
        res.json({spaceId})
    }
    catch(error: any){
        res.status(500).send(error.message)
    }
})

/**
 * headers:
 *     authorization: 'Bearer [accessToken]'
 * body:
 * {
 *     spaceId: string,
 *     name: string,
 *     invite: string[],
 *     serverId: string
 * }
 * response:
 * {
 *     roomId: string
 * }
 */
app.post('/createRoom', async (req, res) => {
    const name: string = req.body.name
    const invite: string[] = req.body.invite
    const spaceId: string = req.body.spaceId
    const serverId: string = req.body.serverId

    try{
        var client = authenticateUser(req.headers.authorization)!
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }

    if(!name || !invite || !spaceId || !serverId)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following fields: spaceId, name, invite, serverId')

    try{
        const guild = await client.guilds.fetch(serverId)
        const category = await guild.channels.fetch(spaceId)
        if(!category)
            throw new Error('Unknown space')
        if(category.type !== 'GUILD_CATEGORY')
            throw new Error('Space channel is not of type category')

        const textChannel = await category.createChannel(name, {type: ChannelTypes.GUILD_TEXT})
        const everyoneRole = guild.roles.everyone
        const roomId = textChannel.id
        
        const allMembersOfCategory: string[] = []
        for(const permission of category.permissionOverwrites.cache){
            if(permission[1].type === 'member')
                allMembersOfCategory.push(permission[0])
        }

        //By default all members of the category (space) have the permission to access the newly created channel (room)
        //Thus revoke their permissions if they are not invited and not the creator of this room
        for(const member of allMembersOfCategory)
            if(!invite.includes(member) && client.user!.id !== member)
                await textChannel.permissionOverwrites.delete(member)

        await textChannel.permissionOverwrites.create(everyoneRole, {
            'VIEW_CHANNEL': false
        })
        
        res.json({roomId})
    }
    catch(error: any){
        res.status(500).send(error)
    }
})

/**
 * headers:
 *     authorization: 'Bearer [accessToken]'
 * body:
 * {
 *     roomId: string,
 *     serverId: string
 * }
 */
app.post('/leaveRoom', async (req, res) => {
    const roomId: string = req.body.roomId
    const serverId: string = req.body.serverId

    try{
        var client = authenticateUser(req.headers.authorization)!
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }

    if(!roomId || !serverId)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following fields: roomId, serverId')

    try{
        const guild = await client.guilds.fetch(serverId)
        const channel = await guild.channels.fetch(roomId)
        if(!channel)
            throw new Error('Channel not found')
        if(channel.type !== 'GUILD_TEXT')
            throw new Error('You can only leave text channels')
        await channel.permissionOverwrites.delete(client.user!.id)
        res.send('Successfully left room ' + roomId)
    }
    catch(error: any){
        res.status(500).send(error)
    }
})

/**
 * headers:
 *     authorization: 'Bearer [accessToken]'
 * body:
 * {
 *     roomId: string,
 *     invite: string[],
 *     serverId: string
 * }
 */
app.post('/inviteToRoom', async (req, res) => {
    const roomId: string = req.body.roomId
    const invite: string[] = req.body.invite
    const serverId: string = req.body.serverId

    try{
        var client = authenticateUser(req.headers.authorization)!
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }
    
    if(!roomId || !invite || !serverId)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following fields: roomId, invite, serverId')

    try{
        const guild = await client.guilds.fetch(serverId)
        const channel = await guild.channels.fetch(roomId)
        if(!channel)
            throw new Error('Channel not found')
        if(channel.type !== 'GUILD_TEXT')
            throw new Error('You can only invite to text channels')
        for(const participant of invite)
            await channel.permissionOverwrites.create(participant, {'VIEW_CHANNEL': true})
        res.send('Invitation successful')
    }
    catch(error: any){
        res.status(500).send(error)
    }
})

/**
 * params:
 *     serverId
 * headers:
 *     authorization = 'Bearer [accessToken]'
 */
app.get('/rooms/:serverId', async (req, res) => {
    const serverId: string = req.params.serverId

    if(!serverId || serverId === '')
        return res.status(400).send('Server ID missing')

    try{
        var client = authenticateUser(req.headers.authorization)
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }

    const rooms = await getRooms(client, serverId)
    res.json(rooms)
})

async function getRooms(client: Client, serverId: string){
    type RoomType = {
        roomId: string,
        name: string,
        gar?: string,
        childRooms?: ChildType[]
    }

    type ChildType = {
        roomId: string,
        name: string
    }

    const rooms: RoomType[] = []
    const channels = (await client.guilds.fetch(serverId!)).channels.cache.filter(channel => channel.viewable)
    for(const channel of channels){
        if(channel[1].type === 'GUILD_CATEGORY'){
            const splitName = channel[1].name.split('|')
            const gar = splitName.pop()
            const name = splitName.join('|')
            rooms.push({
                roomId: channel[0],
                name,
                gar,
                childRooms: []
            })
        }
    }

    for(const channel of channels){
        if(channel[1].type === 'GUILD_TEXT'){
            const parentRoom = channel[1].parentId
            if(parentRoom){
                const index = indexOf(rooms, parentRoom)
                if(index > -1)
                    rooms[index].childRooms?.push({
                        roomId: channel[0],
                        name: channel[1].name
                    })
                else
                    rooms.push({
                        roomId: channel[0],
                        name: channel[1].name
                    })
            }
            else
                rooms.push({
                    roomId: channel[0],
                    name: channel[1].name
                })
        }
    }

    const dmChannels = client.channels.cache.filter(channel => channel.type === 'DM')
    for(const dmChannel of dmChannels)
        rooms.push({
            roomId: dmChannel[0],
            name: (dmChannel[1] as DMChannel).recipient.displayName
        })

    return rooms

    function indexOf(rooms: RoomType[], roomId: string){
        for(const [index, room] of rooms.entries())
            if(room.roomId === roomId)
                return index
        return -1
    }
}

/**
 * Important!! Only call this endpoint with the access token of the creator of the server because some members may not have access to all rooms!
 * Deletes all rooms (channels) of the server.
 * This is only for development purposes because when the dpex server restarts all alliances are gone.
 * This request especially provides a way to delete all the spaces (categories) that were associated with those former alliances.
 * headers:
 *     authorization = 'Bearer [accessToken]'
 * body:
 *     serverId: string
 */
app.delete('/deleteAllRooms', async (req, res) => {
    const serverId = req.body.serverId

    if(!serverId)
        return res.status(400).send('Request body is not well formed. Make sure to provide the following field: serverId')

    try{
        var client = authenticateUser(req.headers.authorization)
    }
    catch(error: any){
        return res.status(401).send(error.message)
    }
    const guild = await client.guilds.fetch(serverId)
    for(const channel of guild.channels.cache){
        try{
            channel[1].delete()
        }
        catch(error: any){
            console.error(error.message)
        }
    }
    res.send('Deleted all rooms')
})

function authenticateUser(authHeader?: string){
    if(!authHeader)
        throw new Error('Authorization header missing')
    const splitAuthHeader = authHeader.split(' ')
    const bearer = splitAuthHeader[0]
    const accessToken = splitAuthHeader[1]

    if(bearer !== 'Bearer' || !accessToken)
        throw new Error('Authorization header is malformed. Please provide it with \'Bearer [accessToken]\'')

    const client = clientMap.get(accessToken as string)
    if(!client)
        throw new Error('Unknown access token')
    return client
}