# dpex-communication-discord-adapter

This application is based on the ExpressJS framwork. It provides a REST API for the dpex framework to send and receive messages via discord.

## Prerequesites

Node v18.17.0

## Install the dependencies

## Start the application

> ```npm start [PORT_NUMBER] [COMMUNICATION_MODULE_NAME]```

* ```PORT_NUMBER```: The port on which the server will run
* ```COMMUNICATION_MODULE_NAME```: The name of the corresponding communication connector in the dpex-application.

For example:

> ```npm start 3002 Discord```

## Environment variables

The application needs the following environment variables:

* ```RECEIVE_MESSAGE_ENDPOINT```: The absolute URL of the dpex application to which a receive message request should be sent via REST (e.g. ```http://localhost:8080/incoming/receiveMessage```)

## Important information

* The Discord Adapter is only working on a single Discord Server. Therefore the Discord accounts of all process participants have to be manually added to the Discord Server before working with dpex!
* The Org Model has to specify the *Discord IDs*. You can only access your Discord ID with the developer options enabled. Not to be confused with the Username or Nickname! The Discord ID contains only digets!

## How to configure the Discord Adapter to run on another Discord Server

If, for an reason, you want the Discord Adapter to work with another Discord Server follow these steps:

1. Create a new Discord Server. __Important:__ The admin of the Discord Server cannot be used in DPEX later because this account has access to all private rooms which is undesired! Thus, create the server with an account that won't be used during process execution.
2. Configure the ```@everyone``` role: 
    - Right click on the server → Server Settings → Roles
    - Select Default Permissions → @everyone
    - Check all permissions **except** _Administrator_
3. Get the server ID:
    - Open User Settings by clicking on the gear icon next to your user name
    - Select Advanced and activate developer mode
    - Right click on the server → Copy Server ID
    - Paste the server ID in the corresponding field in the dpex frontend when creating the Discord connector
4. Invite the Discord accounts from the organizational model to the newly created server

## How do I log into my Discord account via DPEX?

1. Log into Discord via your web browser.
2. Paste the following lines of code in the console:

    ```
    window.webpackChunkdiscord_app.push([
    [Math.random()],
    {},
    req => {
        for (const m of Object.keys(req.c)
        .map(x => req.c[x].exports)
        .filter(x => x)) {
        if (m.default && m.default.getToken !== undefined) {
            return copy(m.default.getToken());
        }
        if (m.getToken !== undefined) {
            return copy(m.getToken());
        }
        }
    },
    ]);
    console.log('%cWorked!', 'font-size: 50px');
    console.log(`%cYou now have your token in the clipboard!`, 'font-size: 16px');
    ```

    This will copy your access token into your clipboard (no scam, trust us😉).
3. Copy the access token in the _Password/Access Token_ field on the Discord login page in the DPEX frontend.