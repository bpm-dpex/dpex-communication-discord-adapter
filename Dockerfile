# Use an official Node.js runtime as the base image
FROM node:18-alpine

# Create a directory to hold the application code inside the image
WORKDIR /app

# Copy the application files to the container
COPY package*.json ./

# Install the application dependencies including devDependencies
# Dev dependencies are needed for the build process
RUN npm install

COPY . .

# Compile TypeScript to JavaScript
# Uncomment below if you use tsc for building your project
RUN npm run build

# Expose the port the app runs on
EXPOSE 3002

# Define the command to run the application using ts-node
CMD ["npm", "run", "start:prod", "--", "3002", "Discord"]
